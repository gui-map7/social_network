# Social Network App

This is a simulation of a social network app, much like how Facebook Messenger works. It does not feature a login feature, so the user can "control" each person in the database.  
Functionalities:  
- add/remove friends  
- initiate conversations  
- filter conversations/friends 

Developed by:  
Enache Alexandru  
Frenț Paul-Grigore