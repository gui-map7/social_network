package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.MesajValidator;
import socialnetwork.domain.validators.PrietenieValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.MesajFile;
import socialnetwork.repository.file.PrietenieFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.UtilizatorService;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("mainPage.fxml"));
        AnchorPane root=loader.load();

        Controller ctrl=loader.getController();

        primaryStage.setScene(new Scene(root, 700, 500));
        primaryStage.setTitle("SocialNetwork");
        primaryStage.show();

        String fileNameUsers = "data/users.csv";
        String fileNameFriendships = "data/friends_date.csv";
        String fileNameMessages = "data/messages.csv";
        String fileNamePendingFriendships = "data/friends_date_pending.csv";
        Repository<Long, Utilizator> userFileRepository = new UtilizatorFile(fileNameUsers, new UtilizatorValidator());
        Repository<Tuple<Long, Long>, Prietenie> friendFileRepository = new PrietenieFile(fileNameFriendships, new PrietenieValidator());
        MesajFile messageFileRepository = new MesajFile(fileNameMessages, new MesajValidator());
        Repository<Tuple<Long, Long>, Prietenie> friendFilePendingRepository = new PrietenieFile(fileNamePendingFriendships, new PrietenieValidator());
        UtilizatorService service_utilizator = new UtilizatorService(userFileRepository, friendFileRepository, messageFileRepository, friendFilePendingRepository);


        ctrl.setService(service_utilizator);


    }


    public static void main(String[] args) {
        launch(args);
    }
}
