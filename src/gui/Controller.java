package gui;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Popup;
import javafx.stage.Stage;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.RequestDTO;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.service.UtilizatorService;

import javafx.scene.control.TextField;

import java.security.PrivilegedExceptionAction;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Controller {

    private UtilizatorService service;
    ObservableList<Utilizator> modelUsersList = FXCollections.observableArrayList();
    ObservableList<Utilizator> modelFriendsList = FXCollections.observableArrayList();
    ObservableList<Utilizator> modelNonFriendsList = FXCollections.observableArrayList();
    ObservableList<RequestDTO> modelRequestsList = FXCollections.observableArrayList();
    ObservableList<RequestDTO> modelRequestsSentList = FXCollections.observableArrayList();

    @FXML
    TextField textFieldSearchUser;

    @FXML
    TableView<Utilizator> usersTableView;
    @FXML
    TableView<Utilizator> friendsTableView;
    @FXML
    TableView<Utilizator> nonFriendsTableView;
    @FXML
    TableView<RequestDTO> requestsTableView;
    @FXML
    TableView<RequestDTO> requestsSentTableView;

    @FXML
    TableColumn<Utilizator, Long> tableColumnUserID;
    @FXML
    TableColumn<Utilizator, String> tableColumnUserName;
    @FXML
    TableColumn<Utilizator, String> tableColumnUserLastName;

    @FXML
    TableColumn<Utilizator, Long> tableColumnFriendID;
    @FXML
    TableColumn<Utilizator, String> tableColumnFriendName;
    @FXML
    TableColumn<Utilizator, String> tableColumnFriendLastName;

    @FXML
    TableColumn<Utilizator, Long> tableColumnNonFriendID;
    @FXML
    TableColumn<Utilizator, String> tableColumnNonFriendName;
    @FXML
    TableColumn<Utilizator, String> tableColumnNonFriendLastName;

    @FXML
    TableColumn<RequestDTO, String> tableColumnRequestID;
    @FXML
    TableColumn<RequestDTO, String> tableColumnRequestName;
    @FXML
    TableColumn<RequestDTO, String> tableColumnRequestLastName;
    @FXML
    TableColumn<RequestDTO, LocalDateTime> tableColumnRequestDate;

    @FXML
    TableColumn<RequestDTO, String> tableColumnRequestSentID;
    @FXML
    TableColumn<RequestDTO, String> tableColumnRequestSentName;
    @FXML
    TableColumn<RequestDTO, String> tableColumnRequestSentLastName;
    @FXML
    TableColumn<RequestDTO, LocalDateTime> tableColumnRequestSentDate;

    @FXML
    public void initialize(){
        tableColumnUserID.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnUserName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnUserLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableColumnFriendID.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnFriendName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnFriendLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableColumnNonFriendID.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnNonFriendName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnNonFriendLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableColumnRequestID.setCellValueFactory(new PropertyValueFactory<>("userId"));
        tableColumnRequestName.setCellValueFactory(new PropertyValueFactory<>("userFirstName"));
        tableColumnRequestLastName.setCellValueFactory(new PropertyValueFactory<>("userLastName"));
        tableColumnRequestDate.setCellValueFactory(new PropertyValueFactory<>("date"));

        tableColumnRequestSentID.setCellValueFactory(new PropertyValueFactory<>("userId"));
        tableColumnRequestSentName.setCellValueFactory(new PropertyValueFactory<>("userFirstName"));
        tableColumnRequestSentLastName.setCellValueFactory(new PropertyValueFactory<>("userLastName"));
        tableColumnRequestSentDate.setCellValueFactory(new PropertyValueFactory<>("date"));

        usersTableView.setItems(modelUsersList);
        friendsTableView.setItems(modelFriendsList);
        nonFriendsTableView.setItems(modelNonFriendsList);
        requestsTableView.setItems(modelRequestsList);
        requestsSentTableView.setItems(modelRequestsSentList);

        textFieldSearchUser.textProperty().addListener(x -> handleFilter());
    }

    private void handleFilter() {
        Predicate<Utilizator> namePredicate = x -> x.getFirstName().startsWith(textFieldSearchUser.getText());
        modelUsersList.setAll(
                StreamSupport.stream(service.getAll().spliterator(), false)
                .filter(namePredicate)
                .collect(Collectors.toList()));
    }

    private void refreshListsUser(Utilizator selected){
        List<Utilizator> usersList = StreamSupport
                .stream(service.getAll().spliterator(), false)
                .collect(Collectors.toList());

        List<Prietenie> requestsList = StreamSupport
                .stream(service.getAllRequests().spliterator(), false)
                .collect(Collectors.toList());

        modelFriendsList.setAll(selected.getFriends());

        modelNonFriendsList.setAll(usersList
                .stream()
                .filter(x -> !selected.getFriends().contains(x) && !x.equals(selected))
                .collect(Collectors.toList()));

        modelRequestsList.setAll(requestsList
                .stream()
                .filter(x -> x.getId().getRight().equals(selected.getId()))
                .map(x -> new RequestDTO(service.findOne(x.getId().getLeft()), x.getDate()))
                .collect(Collectors.toList()));

        modelRequestsSentList.setAll(requestsList
                .stream()
                .filter(x -> x.getId().getLeft().equals(selected.getId()))
                .map(x -> new RequestDTO(service.findOne(x.getId().getRight()), x.getDate()))
                .collect(Collectors.toList()));
    }

    public void handleCompleteTables(MouseEvent mouseEvent){
        Utilizator selected = usersTableView.getSelectionModel().getSelectedItem();
        System.out.println("Selected user: " + selected);
        refreshListsUser(selected);
    }

    public void handleDeleteFriend(ActionEvent mouseEvent){
        Utilizator selectedUser = usersTableView.getSelectionModel().getSelectedItem();
        Utilizator selectedFriend = friendsTableView.getSelectionModel().getSelectedItem();
        if(selectedUser != null && selectedFriend != null){
            Prietenie deleted = service.deletePrietenie(selectedUser.getId(), selectedFriend.getId());
            if(deleted != null) {
                refreshListsUser(selectedUser);
                System.out.println("FRIENDSHIP DELETED. OK.");
            }
        }
        else{
            System.out.println("MISSING ARGUMENT(S).");
        }
    }

    public void handleAddRequest(ActionEvent mouseEvent){
        try{
            Utilizator selectedUser = usersTableView.getSelectionModel().getSelectedItem();
            Utilizator selectedNonFriend = nonFriendsTableView.getSelectionModel().getSelectedItem();
            if(selectedUser != null && selectedNonFriend != null){
                service.addCererePrietenie(selectedUser.getId(), selectedNonFriend.getId());
                refreshListsUser(selectedUser);
                System.out.println("REQUEST ADDED. OK.");
                return;
            }
            System.out.println("MISSING ARGUMENT(S).");
        }
        catch (ServiceException ex){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText(null);
            alert.setContentText(ex.getMessage());
            alert.showAndWait();
        }
    }

    public void handleAcceptRequest(ActionEvent mouseEvent){
        Utilizator selectedUser = usersTableView.getSelectionModel().getSelectedItem();
        RequestDTO selectedRequest = requestsTableView.getSelectionModel().getSelectedItem();
        if(selectedUser != null && selectedRequest != null){
            service.AcceptaCererePrietenie(selectedRequest.getUserId(), selectedUser.getId());
            refreshListsUser(selectedUser);
            System.out.println("REQUEST ACCEPTED. OK.");
            return;
        }
        System.out.println("MISSING ARGUMENT(S).");
    }

    public void handleRejectRequest(ActionEvent mouseEvent){
        Utilizator selectedUser = usersTableView.getSelectionModel().getSelectedItem();
        RequestDTO selectedRequest = requestsTableView.getSelectionModel().getSelectedItem();
        if(selectedUser != null && selectedRequest != null){
            service.RespingeCererePrietenie(selectedRequest.getUserId(), selectedUser.getId());
            refreshListsUser(selectedUser);
            System.out.println("REQUEST REJECTED. OK.");
            return;
        }
        System.out.println("MISSING ARGUMENT(S).");
    }

    public void handleCancelRequest(ActionEvent mouseEvent){
        Utilizator selectedUser = usersTableView.getSelectionModel().getSelectedItem();
        RequestDTO selectedRequest = requestsSentTableView.getSelectionModel().getSelectedItem();
        if(selectedUser != null && selectedRequest != null){
            service.RespingeCererePrietenie(selectedUser.getId(), selectedRequest.getUserId());
            refreshListsUser(selectedUser);
            System.out.println("REQUEST CANCELLED. OK.");
            return;
        }
        System.out.println("MISSING ARGUMENT(S).");
    }


    public void setService(UtilizatorService service){
        this.service = service;
        modelUsersList.setAll(
                StreamSupport
                .stream(service.getAll().spliterator(), false)
                .collect(Collectors.toList()));

    }

    public void handleStartConversation(ActionEvent mouseEvent) throws Exception {


        Utilizator selectedUser = usersTableView.getSelectionModel().getSelectedItem();
        Utilizator selectedFriend = friendsTableView.getSelectionModel().getSelectedItem();

        if(selectedFriend!=null && selectedUser!=null){
            Stage secondStage = new Stage();
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(getClass().getResource("conversationPage.fxml"));
            AnchorPane root=loader.load();
            ConversationController ctrl=loader.getController();

            secondStage.setScene(new Scene(root, 600, 400));
            secondStage.setTitle("SocialNetwork");
            secondStage.show();
            ctrl.setConversation(selectedUser, selectedFriend,service);
            ctrl.setMessages();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText(null);
            alert.setContentText("Nu sunt selectati 2 utilizatori!");
            alert.showAndWait();
        }

    }
}
