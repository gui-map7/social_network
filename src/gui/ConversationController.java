package gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.Message;
import socialnetwork.domain.MessageDTO;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.service.UtilizatorService;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ConversationController {

    private Utilizator u1;
    private Utilizator u2;
    private UtilizatorService s;

    ObservableList<MessageDTO> msg = FXCollections.observableArrayList();

    @FXML
    TableView<MessageDTO> viewMessages;
    @FXML
    TableColumn<MessageDTO,String> userColumn;
    @FXML
    TableColumn<MessageDTO,String > msgColumn;
    @FXML
    TableColumn<MessageDTO,String > dateColumn;
    @FXML
    Button userSender;
    @FXML
    Button friendSender;
    @FXML
    TextField msgField;


    @FXML
    public void initialize(){

        userColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
        msgColumn.setCellValueFactory(new PropertyValueFactory<>("message"));
        dateColumn.setCellValueFactory((new PropertyValueFactory<>("date")));

        viewMessages.setItems(msg);

    }

    public void setConversation(Utilizator u1, Utilizator u2,UtilizatorService s){
        this.u1=u1;
        this.u2=u2;
        this.s=s;
        userSender.setText("Trimite catre "+u2.getLastName());
        friendSender.setText("Trimite catre "+u1.getLastName());
    }



    public void setMessages(){
        try{
            msg.setAll(s.conversatie(u1.getId(),u2.getId()));
        }
        catch(ServiceException ignored){ }
    }


    public void handleSendUser(MouseEvent mouseEvent) {
        String str = msgField.getText();
        HashSet<String> set= new  HashSet<String>();
        set.add(u2.getId().toString());
        this.s.addMesaj(u1.getId(),str,set);
        msgField.setText("");
        setMessages();
    }

    public void handleSendFriend(MouseEvent mouseEvent) {
        String str = msgField.getText();
        HashSet<String> set= new  HashSet<String>();
        set.add(u1.getId().toString());
        this.s.addMesaj(u2.getId(),str,set);
        msgField.setText("");
        setMessages();
    }
}
