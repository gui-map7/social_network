package socialnetwork.service.graf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Graph {
    private final Map<Node, List<Node>> adjacentNodes = new HashMap<>();

    //Nodes from fileNameNodes will only extract from the first column
    //Nodes from fileNameEdges will only extract from the first two columns
    public Graph(String filenameNodes, String filenameEdges){
        extractNodes(filenameNodes);
        extractEdges(filenameEdges);
    }

    public Graph() {}

    private void extractNodes(String filename){
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while((line = br.readLine()) != null){
                List<String> attr = Arrays.asList(line.split(";"));
                if(attr.size() < 1){
                    throw new IOException(filename + " must contain at least a column!\n");
                }
                addNode(attr.get(0));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void extractEdges(String filename){
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while((line = br.readLine()) != null){
                List<String> attr = Arrays.asList(line.split(";"));
                if(attr.size() < 2){
                    throw new IOException(filename + " must contain at least 2 columns!\n");
                }
                addEdge(attr.get(0), attr.get(1));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addNode(String name){
        adjacentNodes.put(new Node(name), new ArrayList<>());
    }

    public void addEdge(String name1, String name2){
        Node node1 = new Node(name1);
        Node node2 = new Node(name2);
        adjacentNodes.get(node1).add(node2);
        adjacentNodes.get(node2).add(node1);
    }

    //returneaza numarul componentelor conexe
    public int numberOfConnectedComponents(){
        int number = 0;
        Map<Node, Boolean> isVisited = new HashMap<>();
        //marcare fiecare nod ca fiind nevizitat
        for(Node node : adjacentNodes.keySet()){
            isVisited.put(node, false);
        }
        //daca nu mai sunt noduri nevizitate, inseamna ca am gasit toate componentele conexe
        while(!isVisited.isEmpty()){
            //se alege un nod nevizitat
            DFS(isVisited.entrySet().stream().findFirst().get().getKey(), isVisited);
            //nodurile marcate ca fiind vizitate au facut parte dintr-o componenta conexa; din acest motiv, le vom sterge
            //pentru a continua cautarea unei alte componente conexe
            isVisited.values().removeIf(isTrue -> isTrue);
            number++;
        }
        return number;
    }

    //DFS clasic iterativ
    private void DFS(Node root, Map<Node, Boolean> isVisited){
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()){
            Node current = stack.pop();
            isVisited.replace(current, true);
            for(Node neighbour : adjacentNodes.get(current)){
                if(!isVisited.get(neighbour)){
                    stack.push(neighbour);
                }
            }
        }
    }

/*    public int longestPathConnectedComponents(){
        int maxLength = 0;
        Map<Node, Boolean> isVisited = new HashMap<>();
        Map<Node, Integer> dp = new HashMap<>();
        for(Node eachNode : adjacentNodes.keySet()){
            for(Node node : adjacentNodes.keySet()){
                isVisited.put(node, false);
            }
            for(Node node : adjacentNodes.keySet()){
                dp.put(node, 0);
            }
            DFS_withLength(dp, isVisited, eachNode);
            for(Node node : dp.keySet()){
                maxLength = Math.max(dp.get(node), maxLength);
            }
        }
        return maxLength;
    }

    private void DFS_withLength(Map<Node, Integer> dp, Map<Node, Boolean> isVisited, Node root){
        if(isVisited.get(root)){
            return;
        }
        isVisited.replace(root, true);
        for(Node neighbour : adjacentNodes.get(root)){
            if(!isVisited.get(neighbour)){
                DFS_withLength(dp, isVisited, neighbour);
            }
            dp.replace(root, Math.max(dp.get(root), dp.get(neighbour) + 1));
        }
    }*/

    //returneaza o lista care are nodurile din componenta conexa cu cel mai lung drum.
    //la sfarsitul acestei liste se adauga lungimea celui mai lung drum
    public List<Node> longestPathConnectedComponents(){
        int[] maxLength = { 0 };
        //componenta conexa candidata
        List<Node> connectedComponent = new ArrayList<>();
        //componenta conexa solutie
        List<Node> bestConnectedComponent = new ArrayList<>();
        Map<Node, Boolean> isVisited = new HashMap<>();
        //vom lua fiecare nod pentru a gasi lungimea maxima intre toate celelalte noduri
        for(Node eachNode : adjacentNodes.keySet()){
            //pregatire pentru urmatoare componenta conexa candidata
            connectedComponent.clear();
            int isChangedMaxLength = maxLength[0];
            //marcare fiecare nod ca fiind nevizitat pentru o noua iteratie
            for(Node node : adjacentNodes.keySet()){
                isVisited.put(node, false);
            }
            DFS_withLength(eachNode, isVisited, 0, maxLength, connectedComponent);

            //verificare si aici pentru a actualiza componenta conexa solutie
            if(maxLength[0] > isChangedMaxLength){
                //componenta conexa candidata devine solutie
                bestConnectedComponent = new ArrayList<>(connectedComponent);
            }
        }
        bestConnectedComponent = bestConnectedComponent.stream().distinct().collect(Collectors.toList());
        //adaugare la sfarsit cel mai lung drum
        bestConnectedComponent.add(new Node(String.valueOf(maxLength[0])));
        return bestConnectedComponent;
    }

    //previousLength - suma lungimilor pana la nodul curent
    private void DFS_withLength(Node root, Map<Node, Boolean> isVisited, int previousLength, int[] maxLength, List<Node> connectedComponent){
        //marcare nod curent ca fiind vizitat
        isVisited.replace(root, true);
        //adaugare nod la componenta conexa
        connectedComponent.add(root);
        int currentLength = 0;
        //iterare prin toate nodurile adiacente ale nodului curent
        for(Node neighbour : adjacentNodes.get(root)){
            //daca un nod adiacent este nevizitat
            if(!isVisited.get(neighbour)){
                //lungimea toatala de la nodul radacina pana la adiacentul lui
                currentLength = previousLength + 1;

                //facem o copie de visited pentru fiecare nod
                Map<Node, Boolean> isVisitedCurrent = new HashMap<>(isVisited);
                isVisitedCurrent.replace(neighbour, true);
                DFS_withLength(neighbour, isVisitedCurrent, currentLength, maxLength, connectedComponent);

                //apelare DFS recursiv pentru nodul adiacent
                //DFS_withLength(neighbour, isVisited, currentLength, maxLength, connectedComponent);
            }
            //daca lungimea toatala este mai mare decat lungimea maxima gasita la nivelul tuturor iteratiilor, se actualizeaza
            if(maxLength[0] < currentLength){
                maxLength[0] = currentLength;
            }
        }
    }

}
