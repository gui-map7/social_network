package socialnetwork.service.graf;

public class TestGraph {

    public static void main(String[] args){
        Graph graph1 = new Graph("data/test_nodes_1.txt", "data/test_edges_1.txt");
        Graph graph2 = new Graph("data/test_nodes_2.txt", "data/test_edges_2.txt");
        System.out.println(graph1.numberOfConnectedComponents());
        System.out.println(graph1.longestPathConnectedComponents());
        //System.out.println(graph1.longestPathConnectedComponents());

    }

}
