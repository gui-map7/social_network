package socialnetwork.service.graf;

import java.util.Objects;

public class Node {
    private final String name;

    public Node(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node that = (Node) o;
        return getName().equals(that.getName());
    }

    @Override
    public String toString(){
        return this.name;
    }

    @Override
    public int hashCode(){
        return Objects.hash(getName());
    }

}
