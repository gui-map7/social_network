package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.MesajFile;
import socialnetwork.service.graf.Graph;
import socialnetwork.service.graf.Node;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UtilizatorService  {
    private final Repository<Long, Utilizator> repo_user;
    private final Repository<Tuple<Long, Long>, Prietenie> repo_friend;
    private final MesajFile repo_message;
    private final Repository<Tuple<Long, Long>, Prietenie> repo_friend_pending;

    public UtilizatorService(Repository<Long, Utilizator> repo_user, Repository<Tuple<Long, Long>, Prietenie> repo_friend, MesajFile repo_message, Repository<Tuple<Long, Long>, Prietenie> repo_friend_pending) {
        this.repo_user = repo_user;
        this.repo_friend = repo_friend;
        this.repo_message = repo_message;
        this.repo_friend_pending = repo_friend_pending;
        addPrieteniiToUtilizatori();
        correctMessages();
    }

    private void addPrieteniiToUtilizatori(){
        for(Prietenie prietenie : repo_friend.findAll()){
            Utilizator user1 = repo_user.findOne(prietenie.getId().getLeft());
            Utilizator user2 = repo_user.findOne(prietenie.getId().getRight());
            user1.getFriends().add(user2);
            user2.getFriends().add(user1);
        }
    }

    private void correctMessages(){
        for(Message message : repo_message.findAll()){
            message.setFrom(repo_user.findOne(message.getFrom().getId()));
            message.setTo(message.getTo().stream().map(u -> repo_user.findOne(u.getId())).collect(Collectors.toList()));
        }
    }

    public Utilizator addUtilizator(Utilizator messageTask) {
        return repo_user.save(messageTask);
    }

    public Utilizator deleteUtilizator(Long id){
        Utilizator deletedUser = repo_user.findOne(id);
        if(deletedUser == null){
            return null;
        }
        while(!deletedUser.getFriends().isEmpty()){
            deletePrietenie(id, deletedUser.getFriends().get(0).getId());
        }
        deleteCererePrietenie(id);
        deleteMesajUtilizator(id);
        return repo_user.delete(id);
    }

   public Prietenie addPrietenie(Long id1, Long id2) {
       Utilizator user1 = repo_user.findOne(id1);
       Utilizator user2 = repo_user.findOne(id2);
       if (user1 == null || user2 == null) {
           throw new IllegalArgumentException("Unul dintre utilizatori nu exista!\n");
       }
       if(id1 > id2){
           long aux = id1;
           id1 = id2;
           id2 = aux;
       }
       Prietenie friendship = new Prietenie();
       friendship.setId(new Tuple<>(id1, id2));
       if (repo_friend.findOne(friendship.getId()) != null) {
           throw new ServiceException("Aceasta prietenie exista deja!\n");
       }
       Prietenie checkFriendship = repo_friend.save(friendship);
       if (checkFriendship != null) {
           return checkFriendship;
       }
       user1.getFriends().add(user2);
       user2.getFriends().add(user1);
       return null;
   }

    public Prietenie deletePrietenie(Long id1, Long id2){
        Utilizator user1 = repo_user.findOne(id1);
        Utilizator user2 = repo_user.findOne(id2);
        if(user1 == null || user2 == null){
            throw new IllegalArgumentException("Unul dintre utilizatori nu exista!\n");
        }
        if(id1 > id2){
            long aux = id1;
            id1 = id2;
            id2 = aux;
        }
        Prietenie checkFriendship = repo_friend.delete(new Tuple<>(id1, id2));
        if(checkFriendship != null){
            user1.getFriends().remove(user2);
            user2.getFriends().remove(user1);
            return checkFriendship;
        }
        return null;
    }

    public void deleteCererePrietenie(long idUser){
        while(StreamSupport
                .stream(repo_friend_pending.findAll().spliterator(), false)
                .anyMatch(p -> p.getId().getRight() == idUser || p.getId().getLeft() == idUser)){
            //sterge cererile care contin idUser
            repo_friend_pending.delete(StreamSupport
                    .stream(repo_friend_pending.findAll().spliterator(), false)
                    .filter(p -> p.getId().getRight() == idUser || p.getId().getLeft() == idUser)
                    .findFirst().get().getId());
        }
    }

    public void deleteMesajUtilizator(long idUser){
        while(StreamSupport
                .stream(repo_message.findAll().spliterator(), false)
                .anyMatch(p -> p.getFrom().getId() == idUser)){
            //stergere mesaje care au idFrom == idUser
            repo_message.delete(StreamSupport
                    .stream(repo_message.findAll().spliterator(), false)
                    .filter(p -> p.getFrom().getId() == idUser)
                    .findFirst().get().getId());
        }
        while(StreamSupport
                .stream(repo_message.findAll().spliterator(), false)
                .anyMatch(p -> p.getTo().contains(repo_user.findOne(idUser)))){
            //stergere din listele 'to', idUser
            StreamSupport
                    .stream(repo_message.findAll().spliterator(), false)
                    .filter(p -> p.getTo().contains(repo_user.findOne(idUser)))
                    .findFirst().get().getTo().remove(repo_user.findOne(idUser));
        }
        while(StreamSupport
                .stream(repo_message.findAll().spliterator(), false)
                .anyMatch(p -> p.getTo().isEmpty())){
            //stergere mesaje care au lista 'to' vida
            repo_message.delete(StreamSupport
                    .stream(repo_message.findAll().spliterator(), false)
                    .filter(p -> p.getTo().isEmpty())
                    .findFirst().get().getId());
        }
    }

    public Iterable<Utilizator> getAll(){
        return repo_user.findAll();
    }


    public Utilizator findOne(long id){
        return repo_user.findOne(id);
    }

    public Iterable<Prietenie> getAllRequests(){
        return repo_friend_pending.findAll();
    }

    public Prietenie getPrietenie(Long id1, Long id2){
        Utilizator user1 = repo_user.findOne(id1);
        Utilizator user2 = repo_user.findOne(id2);
        if(user1 == null || user2 == null){
            throw new IllegalArgumentException("Unul dintre utilizatori nu exista!\n");
        }
        if(id1 > id2){
            long aux = id1;
            id1 = id2;
            id2 = aux;
        }
        return repo_friend.findOne(new Tuple<>(id1, id2));
    }

    public void numarComunitati(String fileNameUsers, String fileNameFriendships){
        Graph graph = new Graph(fileNameUsers, fileNameFriendships);
        System.out.println("\nNumarul de comunitati: " + graph.numberOfConnectedComponents() + "\n");
    }

    public void ceaMaiSociabilaComunitate(String fileNameUsers, String fileNameFriendships){
        Graph graph = new Graph(fileNameUsers, fileNameFriendships);
        System.out.println("\nCea mai sociabila comunitate este formata din:");
        List<Node> array = graph.longestPathConnectedComponents();
        for(int i = 0; i < array.size() - 1; i++){
            System.out.print(repo_user.findOne(Long.parseLong(array.get(i).getName())).getFirstName() + ", ");
        }
        System.out.println("\nCel mai lung drum din aceasta comunitate: " + array.get(array.size() - 1) + "\n");

    }

    public Utilizator PrieteniiUtilizator(Long id){
        Utilizator user = repo_user.findOne(id);
        if(user == null){
            return null;
        }
        if(user.getFriends().isEmpty()){
            System.out.println("\nUtilizatorul " + user.getFirstName() + " " + user.getLastName() + " nu are prieteni.\n");
            return user;
        }
        System.out.println("\nUtilizatorul " + user.getFirstName() + " " + user.getLastName() + " are urmatorii prieteni:");
        user.getFriends()
                .stream()
                .map(u -> new PrietenieCuUnUtilizatorDataDTO(u.getFirstName(), u.getLastName(), getPrietenie(user.getId(), u.getId()).getDate()))
                .forEach(System.out::println);
        System.out.println();
        return user;
    }

    public Utilizator PrieteniiUtilizatorLuna(Long id, int month){
        Utilizator user = repo_user.findOne(id);
        if(user == null){
            return null;
        }
        if(user.getFriends().isEmpty()){
            System.out.println("\nUtilizatorul " + user.getFirstName() + " " + user.getLastName() + " nu are prieteni.\n");
            return user;
        }
        System.out.println("\nUtilizatorul " + user.getFirstName() + " " + user.getLastName() + " are urmatorii prieteni facuti in luna " + month + ":");
        Predicate<PrietenieCuUnUtilizatorDataDTO> filterByMonth = u -> u.getDate().getMonthValue() == month;
        user.getFriends()
                .stream()
                .map(u -> new PrietenieCuUnUtilizatorDataDTO(u.getFirstName(), u.getLastName(), getPrietenie(user.getId(), u.getId()).getDate()))
                .filter(filterByMonth)
                .forEach(System.out::println);
        System.out.println();
        return user;
    }

    public void addMesaj(long fromId, String text, Set<String> toIdsString){
        String error = "------------------------\n";
        Utilizator fromUser = repo_user.findOne(fromId);
        if(fromUser == null){
            error += "ID-ul expeditor " + fromId + " nu exista!\n";
        }
        Message message = new Message(fromUser, text, null);
        message.setId(repo_message.getLastAvailableId());
        for(String idDest : toIdsString){
            Utilizator toUser = repo_user.findOne(Long.parseLong(idDest));
            if(toUser == null){
                error += "ID-ul destinatar " + idDest + " nu exista!\n";
            }
            else if(toUser.getId() == fromId){
                error += "ID-ul destinatar " + idDest + " nu poate sa-si trimita un mesaj lui insusi!\n";
            }
            else{
                message.addTo(toUser);
            }
        }
        if(!error.equals("------------------------\n")){
            throw new ServiceException(error);
        }
        repo_message.save(message);
    }

    public void addRaspuns(long fromId, String text, long msgId){
        String error = "------------------------\n";
        Utilizator fromUser = repo_user.findOne(fromId);
        if(fromUser == null){
            error += "ID-ul expeditor " + fromId + " nu exista!\n";
        }
        Message toMessage = repo_message.findOne(msgId);
        if(toMessage == null){
            error += "ID-ul mesajului " + msgId + " nu exista!\n";
        }
        else if(!toMessage.getTo().contains(fromUser)){
            error += "Expeditorul " + fromId + " nu are o conversatie cu destinatarul " + toMessage.getFrom().getId() + "!\n";
        }
        else{
            for(Message message : repo_message.findAll()){
                if(message.getReply() != null){
                    if(message.getFrom().getId() == fromId && message.getReply().getId() == msgId){
                        error += "Expeditorul " + fromId + " a raspuns deja la mesajul " + message.getReply().getId() + "!\n";
                        break;
                    }
                }
            }
        }
        if(!error.equals("------------------------\n")){
            throw new ServiceException(error);
        }
        Message message = new Message(fromUser, text, toMessage);
        message.setId(repo_message.getLastAvailableId());
        message.addTo(toMessage.getFrom());
        repo_message.save(message);
    }

    public List<MessageDTO> conversatie(long id1, long id2) {
        String error = "\n";
        if (repo_user.findOne(id1) == null) {
            error += "Primul utilizator nu exista!\n";
        }
        if (repo_user.findOne(id2) == null) {
            error += "Al doilea utilizator nu exista!\n";
        }
        if(!error.equals("\n")){
            throw new ServiceException(error);
        }
        List<Message> lstMessages = new ArrayList<>();
        repo_message.findAll().forEach(lstMessages::add);
        //System.out.println(lstMessages);
        lstMessages = lstMessages
                .stream()
                //.filter(m -> m.getReply() != null)
                .filter(m -> (m.getFrom().getId() == id1 && m.getTo().get(0).getId() == id2) || (m.getFrom().getId() == id2 && m.getTo().get(0).getId() == id1))
                .collect(Collectors.toList());
        if(lstMessages.isEmpty()){
            throw new ServiceException("\nNu exista conversatii intre acesti 2 utilizatori!\n");
        }
        /*Collections.reverse(lstMessages);
        Stack<Message> conversation = new Stack<>();
        while(!lstMessages.isEmpty()){
            Message message = lstMessages.get(0);
            lstMessages.remove(message);
            conversation.push(message);
            while(message.getReply() != null){
                message = repo_message.findOne(message.getReply().getId());
                conversation.push(message);
                lstMessages.remove(message);
            }
            System.out.println("\n-------------");
            while(!conversation.isEmpty()){
                System.out.println(conversation.pop());
            }
            System.out.println("--------------");
        }*/

        List<MessageDTO> lstMessagesDTO = new ArrayList<MessageDTO>();

        lstMessages.forEach(x->{
            MessageDTO n = new MessageDTO(x);
            lstMessagesDTO.add(n);
        });


        return lstMessagesDTO;
    }

    public void addCererePrietenie(long id1, long id2) {
        Utilizator user1 = repo_user.findOne(id1);
        Utilizator user2 = repo_user.findOne(id2);
        if (user1 == null || user2 == null) {
            throw new IllegalArgumentException("Unul dintre utilizatori nu exista!\n");
        }

        Prietenie friendship = new Prietenie();
        friendship.setId(new Tuple<>(id1, id2));
        Prietenie reverseFriendship = new Prietenie();
        reverseFriendship.setId(new Tuple<>(id2, id1));
        if(id1 > id2){
            if (repo_friend.findOne(reverseFriendship.getId()) != null) {
                throw new ServiceException("Aceasta prietenie exista deja!\n");
            }
        }
        if (repo_friend.findOne(friendship.getId()) != null) {
            throw new ServiceException("Aceasta prietenie exista deja!\n");
        }
        if(id1 > id2) {
            if (repo_friend_pending.findOne(reverseFriendship.getId()) != null) {
                throw new ServiceException("Exista deja o cerere intre cei doi utilizatori!\n");
            }
        }

        if (repo_friend_pending.findOne(friendship.getId()) != null) {
            throw new ServiceException("Aceasta cerere exista deja!\n");
        }
        repo_friend_pending.save(friendship);
    }

    public void AcceptaCererePrietenie(long id1, long id2) {
        Prietenie friendship = new Prietenie();
        friendship.setId(new Tuple<>(id1, id2));
        if (repo_friend_pending.findOne(friendship.getId()) == null) {
            throw new ServiceException("Aceasta cerere nu exista!\n");
        }
        repo_friend_pending.delete(friendship.getId());
        addPrietenie(id1, id2);
    }

    public void RespingeCererePrietenie(long id1, long id2) {
        Prietenie friendship = new Prietenie();
        friendship.setId(new Tuple<>(id1, id2));
        if (repo_friend_pending.findOne(friendship.getId()) == null) {
            throw new ServiceException("Aceasta cerere nu exista!\n");
        }
        repo_friend_pending.delete(friendship.getId());
    }
}

