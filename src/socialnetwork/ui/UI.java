package socialnetwork.ui;

import socialnetwork.domain.Utilizator;
import socialnetwork.service.UtilizatorService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class UI {
    private final UtilizatorService service;

    public UI(UtilizatorService s) {
        this.service = s;
    }

    public void readUtilizator(){
        Scanner scan = new Scanner(System.in);
        System.out.print("ID: " );
        Long id = scan.nextLong();
        scan.nextLine();
        System.out.print("First Name: ");
        String firstName = scan.nextLine();
        System.out.print("Last Name: ");
        String lastName = scan.nextLine();
        Utilizator user = new Utilizator(firstName, lastName);
        user.setId(id);
        System.out.println();
        if(service.addUtilizator(user) == null){
            throw new IllegalArgumentException("Utilizatorul a fost adaugat cu succes!\n");
        }
        else{
            throw new IllegalArgumentException("Exista deja un utilizator cu acest ID!\n");
        }
    }

    public void readDeleteUtilizator(){
        Scanner scan = new Scanner(System.in);
        System.out.print("ID: " );
        Long id = scan.nextLong();
        scan.nextLine();
        if(service.deleteUtilizator(id) == null){
            throw new IllegalArgumentException("Nu exista un utilizator cu acest ID!\n");
        }
        else{
            throw new IllegalArgumentException("Utilizatorul a fost sters cu succes!\n");
        }
    }

    public void readPrietenie(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Primul ID: " );
        long id1 = scan.nextLong();
        scan.nextLine();
        System.out.print("Al doilea ID: " );
        long id2 = scan.nextLong();
        scan.nextLine();
        if(service.addPrietenie(id1, id2) == null){
            throw new IllegalArgumentException("Prietenia a fost adaugata cu succes!\n");
        }
        else{
            throw new IllegalArgumentException("Aceasta prietenie exista deja!\n");
        }
    }

    public void readDeletePrietenie(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Primul ID: " );
        long id1 = scan.nextLong();
        scan.nextLine();
        System.out.print("Al doilea ID: " );
        long id2 = scan.nextLong();
        scan.nextLine();
        if(service.deletePrietenie(id1, id2) == null){
            throw new IllegalArgumentException("Aceasta prietenie nu exista!\n");
        }
        else{
            throw new IllegalArgumentException("Prietenia a fost stearsa cu succes!\n");
        }
    }

    public void readPrieteniiUtilizator(){
        Scanner scan = new Scanner(System.in);
        System.out.print("ID: " );
        Long id = scan.nextLong();
        scan.nextLine();
        if(service.PrieteniiUtilizator(id) == null){
            throw new IllegalArgumentException("Nu exista un utilizator cu acest ID!\n");
        }
    }

    public void readPrieteniiUtilizatorLuna(){
        Scanner scan = new Scanner(System.in);
        System.out.print("ID: " );
        long id = scan.nextLong();
        scan.nextLine();
        System.out.print("Luna numerica: ");
        int month = scan.nextInt();
        scan.nextLine();
        if(month < 1 || month > 12){
            throw new IllegalArgumentException("Luna trebuie sa fie cuprinsa intre 1 si 12!\n");
        }
        if(service.PrieteniiUtilizatorLuna(id, month) == null){
            throw new IllegalArgumentException("Nu exista un utilizator cu acest ID!\n");
        }
    }

    public void readMesaj(){
        Scanner scan = new Scanner(System.in);
        System.out.print("ID expeditor: ");
        long id = scan.nextLong();
        scan.nextLine();
        System.out.print("Mesaj: ");
        String message = scan.nextLine();
        System.out.print("ID-uri destinatari (separate prin spatiu): ");
        String recipientsId = scan.nextLine();
        String[] recipientsIdString = recipientsId.split(" ");
        service.addMesaj(id, message, new HashSet<>(Arrays.asList(recipientsIdString)));
    }

    public void readRaspuns(){
        Scanner scan = new Scanner(System.in);
        System.out.print("ID expeditor: ");
        long idExp = scan.nextLong();
        scan.nextLine();
        System.out.print("Mesaj: ");
        String message = scan.nextLine();
        System.out.print("ID-ul mesajului la care raspunde: ");
        long idMsg = scan.nextLong();
        scan.nextLine();
        service.addRaspuns(idExp, message, idMsg);
    }

    public void conversatieUI(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Primul ID: " );
        long id1 = scan.nextLong();
        scan.nextLine();
        System.out.print("Al doilea ID: " );
        long id2 = scan.nextLong();
        scan.nextLine();
        service.conversatie(id1, id2);
    }

    public void readCererePrietenie(){
        Scanner scan = new Scanner(System.in);

        System.out.print("ID: " );
        long id1 = scan.nextLong();
        scan.nextLine();
        System.out.print("Catre ID-ul: " );
        long id2 = scan.nextLong();
        scan.nextLine();
        service.addCererePrietenie(id1, id2);
    }

    public void readAcceptaCererePrietenie() {
        Scanner scan = new Scanner(System.in);

        System.out.print("ID: " );
        long id1 = scan.nextLong();
        scan.nextLine();
        System.out.print("Catre ID-ul: " );

        long id2 = scan.nextLong();
        scan.nextLine();
        service.AcceptaCererePrietenie(id1, id2);
    }

    public void readRespingeCererePrietenie() {
        Scanner scan = new Scanner(System.in);

        System.out.print("ID: " );
        long id1 = scan.nextLong();
        scan.nextLine();
        System.out.print("Catre ID-ul: " );
        long id2 = scan.nextLong();
        scan.nextLine();
        service.RespingeCererePrietenie(id1, id2);
    }
}
