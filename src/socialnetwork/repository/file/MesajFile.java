package socialnetwork.repository.file;

import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.utils.Constants;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MesajFile extends AbstractFileRepository<Long, Message> {

    public MesajFile(String fileName, Validator<Message> validator) {
        super(fileName, validator);
    }

    @Override
    public Message extractEntity(List<String> attributes) {
        LocalDateTime date = LocalDateTime.parse(attributes.get(4), Constants.DATE_TIME_SECONDS_FORMATTER);
        String[] toUsersIds = attributes.get(5).split(",");
        List<Utilizator> toUsers = new ArrayList<>();
        for(String toUserId : toUsersIds){
            Utilizator toUser = new Utilizator("", "");
            toUser.setId(Long.parseLong(toUserId));
            toUsers.add(toUser);
        }
        if(attributes.get(3).equals("null")) {
            Utilizator fromUser = new Utilizator("", "");
            fromUser.setId(Long.parseLong(attributes.get(1)));
            Message message = new Message(fromUser, attributes.get(2), null, date, toUsers);
            message.setId(Long.parseLong(attributes.get(0)));
            return message;
        }
        Utilizator fromUser = new Utilizator("", "");
        fromUser.setId(Long.parseLong(attributes.get(1)));
        Message message = new Message(fromUser, attributes.get(2), findOne(Long.parseLong(attributes.get(3))), date, toUsers);
        message.setId(Long.parseLong(attributes.get(0)));
        return message;
    }

    @Override
    protected String createEntityAsString(Message entity) {
        String str = "";
        str += entity.getId() + ";" + entity.getFrom().getId() + ";" + entity.getMessage() + ";";
        if(entity.getReply() == null){
            str += "null;";
        }
        else{
            str += entity.getReply().getId() + ";";
        }
        str += entity.getDate().format(Constants.DATE_TIME_SECONDS_FORMATTER) + ";";
        for(Utilizator u : entity.getTo()){
            str += u.getId() + ",";
        }
        return str;
    }

    public long getLastAvailableId(){
        if(getAll().isEmpty()){
            return 1;
        }
        return Collections.max(getAll().keySet()) + 1;
    }

}
