package socialnetwork.domain;

import socialnetwork.domain.utils.Constants;

import java.time.LocalDateTime;


public class Prietenie extends Entity<Tuple<Long, Long>> {

    LocalDateTime date;

    public Prietenie() {
        this.date = LocalDateTime.now();
    }

    public Prietenie(LocalDateTime date) {
        this.date = date;
    }


    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }


/*    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(!(obj instanceof Prietenie)){
            return false;
        }
        Prietenie that = (Prietenie) obj;
        return (this.getId().getLeft().equals(that.getId().getLeft()) && this.getId().getRight().equals(that.getId().getRight())) ||
                (this.getId().getLeft().equals(that.getId().getRight()) && this.getId().getRight().equals(that.getId().getLeft()));

    }*/

    @Override
    public String toString(){
        return getId().getLeft() + " | " + getId().getRight() + " | " + date.format(Constants.DATE_TIME_FORMATTER);
    }
}
