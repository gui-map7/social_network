package socialnetwork.domain;

import socialnetwork.domain.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {
    private Utilizator from;
    private List<Utilizator> to;
    private final String message;
    private final LocalDateTime date;
    private final Message reply;

    public Message(Utilizator from, String message, Message reply, LocalDateTime date){
        this.from = from;
        this.to = new ArrayList<>();
        this.message = message;
        this.date = date;
        this.reply = reply;
    }

    public Message(Utilizator from, String message, Message reply){
        this.from = from;
        this.to = new ArrayList<>();
        this.message = message;
        this.date = LocalDateTime.now();
        this.reply = reply;
    }

    public Message(Utilizator from, String message, Message reply, LocalDateTime date, List<Utilizator> to){
        this.from = from;
        this.to = new ArrayList<>(to);
        this.message = message;
        this.date = date;
        this.reply = reply;
    }

    public Message(Utilizator from, String message, Message reply, List<Utilizator> to){
        this.from = from;
        this.to = new ArrayList<>(to);
        this.message = message;
        this.date = LocalDateTime.now();
        this.reply = reply;
    }

    public Utilizator getFrom() {
        return from;
    }

    public void setFrom(Utilizator from){
        this.from = from;
    }

    public void setTo(List<Utilizator> to){
        this.to = to;
    }

    public List<Utilizator> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Message getReply() {
        return reply;
    }

    public void addTo(Utilizator user){
        to.add(user);
    }

    @Override
    public String toString(){
        String text = "";
        if(reply != null){
            text += this.getId().toString() + " | " + from.getId() + " | " + message + " | " + reply.getId() + " | " + date.format(Constants.DATE_TIME_SECONDS_FORMATTER) + " | ";
        }
        else{
            text += this.getId().toString() + " | " + from.getId() + " | " + message + " | null | " + date.format(Constants.DATE_TIME_SECONDS_FORMATTER) + " | ";
        }
        for(Utilizator user : to){
            text += user.getId() + " ";
        }
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message that = (Message) o;
        return getFrom().equals(that.getFrom()) &&
                getTo().equals(that.getTo()) &&
                getMessage().equals(that.getMessage()) &&
                getDate().equals(that.getDate()) &&
                getReply().equals(that.getReply());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFrom(), getTo(), getMessage(), getDate(), getReply());
    }
}
