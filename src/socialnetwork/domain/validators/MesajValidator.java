package socialnetwork.domain.validators;

import socialnetwork.domain.Message;

public class MesajValidator implements Validator<Message> {

    @Override
    public void validate(Message entity) throws ValidationException {
        String errors = "";
        if(entity.getMessage().equals("")){
            errors += "Mesaj vid!\n";
        }
        if(!errors.equals("")){
            throw new ValidationException(errors);
        }
    }
}
