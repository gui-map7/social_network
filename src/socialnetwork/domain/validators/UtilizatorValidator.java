package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {

    @Override
    public void validate(Utilizator entity) throws ValidationException {
        String errors = "";
        if(entity.getId() < 1){
            errors += "ID nul sau negativ!\n";
        }
        if(entity.getFirstName().equals("")) {
            errors += "First Name vid!\n";
        } else if(!Character.isUpperCase(entity.getFirstName().charAt(0))){
            errors += "First Name trebuie sa inceapa cu litera mare!\n";
        }
        if(entity.getLastName().equals("")){
            errors += "Last Name vid!\n";
        } else if(!Character.isUpperCase(entity.getLastName().charAt(0))){
            errors += "Last Name trebuie sa inceapa cu litera mare!\n";
        }
        if(!errors.equals("")){
            throw new ValidationException(errors);
        }
    }
}
