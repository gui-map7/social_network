package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;

public class PrietenieValidator implements Validator<Prietenie> {

    @Override
    public void validate(Prietenie entity) throws ValidationException {
        String errors = "";
        if(entity.getId().getLeft() < 1){
            errors += "Primul ID este nul sau negativ!\n";
        }
        if(entity.getId().getRight() < 1){
            errors += "Al doilea ID este nul sau negativ!\n";
        }
        if(entity.getId().getLeft().equals(entity.getId().getRight())){
            errors += "Utilizatorul nu poate fi prieten cu el insusi!\n";
        }
        if(!errors.equals("")){
            throw new ValidationException(errors);
        }
    }
}
