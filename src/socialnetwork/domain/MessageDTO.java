package socialnetwork.domain;


import socialnetwork.domain.utils.Constants;

import java.lang.invoke.ConstantCallSite;

public class MessageDTO {

    public String message;
    public String username;
    public String date;

    public MessageDTO(Message m){
        message=m.getMessage();
        username = m.getFrom().getFirstName()+" "+m.getFrom().getLastName();
        date = m.getDate().format(Constants.DATE_TIME_SECONDS_FORMATTER);
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return username;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
