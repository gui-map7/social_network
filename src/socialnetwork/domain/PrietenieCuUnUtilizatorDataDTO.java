package socialnetwork.domain;

import socialnetwork.domain.utils.Constants;

import java.time.LocalDateTime;

public class PrietenieCuUnUtilizatorDataDTO {
    private final String firstName;
    private final String lastName;
    private final LocalDateTime date;

    public PrietenieCuUnUtilizatorDataDTO(String firstName, String lastName, LocalDateTime date){
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
    }

    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName(){
        return this.lastName;
    }

    public LocalDateTime getDate(){
        return this.date;
    }

    @Override
    public String toString(){
        return this.firstName + " " + this.lastName + " | " + this.date.format(Constants.DATE_TIME_FORMATTER);
    }




}
