package socialnetwork.domain;

import socialnetwork.domain.utils.Constants;

import java.time.LocalDateTime;

public class RequestDTO {
    private final Utilizator user;
    private final String date;

    public RequestDTO(Utilizator user, LocalDateTime date){
        this.user = user;
        this.date = date.format(Constants.DATE_TIME_FORMATTER);
    }

    public Long getUserId(){
        return user.getId();
    }

    public String getUserFirstName(){
        return user.getFirstName();
    }

    public String getUserLastName(){
        return user.getLastName();
    }

    public Utilizator geUser(){
        return user;
    }

    public String getDate(){
        return date;
    }

}
