package socialnetwork;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.MesajFile;
import socialnetwork.repository.file.PrietenieFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.UI;

import java.util.Scanner;

public class Main {

/*
1;Coman;Ionel
2;Apetrei;Ileana
3;Pop;Dan
4;Zaharia;Stancu
5;Froie;Mihai
6;Rica;Robert
7;Catop;Eduard
8;Sheldon;Cooper
9;Leonard;Hofstadter
10;Howard;Wolowitz

5;6;2020-10-14 10:10
1;2;2020-08-15 10:30
3;4;2020-11-17 03:40
4;5;2020-11-08 09:10
5;7;2020-08-12 11:10
1;3;2020-11-02 09:18
1;4;2020-05-25 02:50
5;8;2020-07-08 03:19
5;9;2020-10-12 10:25

3;5;2020-11-14 00:16
3;7;2020-11-14 00:16
5;10;2020-11-14 00:16

1;2;Cum a fost la interviu?;null;2020-11-13 21:45:06;5,7,9,
2;5;M-am descurcat de minune!;1;2020-11-13 21:45:49;2,
3;2;Cum a fost la concurs?;null;2020-11-13 21:46:20;5,7,9,
4;5;Am gresit doar doua grile.;3;2020-11-13 21:46:56;2,
5;2;Sunt mandru de tine!;2;2020-11-13 21:47:37;5,
6;7;Eu zic ca ma accepta!;1;2020-11-13 21:48:23;2,
*/


    private static void printMenu(){
        System.out.println("------------------------------------------");
        System.out.println("u. Afiseaza toti utilizatorii");
        System.out.println("c. Afiseaza toate cererile de prietenie");
        System.out.println("m. Afiseaza toate mesajele");
        System.out.println("1. Adauga utilizator");
        System.out.println("2. Sterge utilizator");
        System.out.println("3. Adauga prietenie intre 2 utilizatori");
        System.out.println("4. Sterge prietenie intre 2 utilizatori");
        System.out.println("5. Afiseaza numarul de comunitati");
        System.out.println("6. Afiseaza cea mai sociabila comunitate");
        System.out.println("7. Afiseaza prieteniile unui utilizator");
        System.out.println("8. Afiseaza prieteniile unui utilizator create intr-o anumita luna");
        System.out.println("9. Creeaza un mesaj de la un utilizator la unul sau mai multi");
        System.out.println("10. Creeaza un reply de la un utilizator la altul");
        System.out.println("11. Afiseaza conversatiile a 2 utilizatori");
        System.out.println("12. Creeaza o cerere de prietenie");
        System.out.println("13. Accepta o cerere de prietenie");
        System.out.println("14. Respinge o cerere de prietenie");
        System.out.println("e. Exit");
        System.out.println("------------------------------------------");
    }

    public static void main(String[] args) {

    }
}



